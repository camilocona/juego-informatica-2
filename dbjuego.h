/*
 * Esta clase realiza las gestiones de conexion y creacion de la base de datos
 * permite la gestion de los datos requeridos para la creacion de nuevos jugadores
 * guardar y cargar partidas modificando y extrayendo los datos
 * de la tabla jugador respectivamente
 */

#ifndef DBJUEGO_H
#define DBJUEGO_H

#include <QtSql>

class  DBJuego
{
public:
    DBJuego();
private:
    QSqlDatabase baseDatos; //variable requerida para la conexion a la DB
    //metodos que permiten la conexion a la DB SQLite de integrada de QT, creacion de DB y tabla respectivamente
    bool conexionBaseDatos(void);
    bool crearBaseDatos(void);
    bool crearTabla();

public slots:
    bool insertar(QString _name); //metodo que realiza el registro de nuevos jugadores
    QSqlQuery consultar(QString _name); //metodo que extrae los datos de los jugadores para realizar el login al juego
    bool guardarPartida(QString _name, int _nivel, int _puntaje, int _vida); //metodo que guarda las partidas modificando los datos del jugador en turno
};

#endif // DBJUEGO_H
