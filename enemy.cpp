#include "enemy.h"

Enemy::Enemy()
{
    //Parametros iniciales
    setRect(0,0,100,100);

    timerDisparar = new QTimer(this);
    connect(timerDisparar, SIGNAL(timeout()),this,SLOT(disparar()));
    timerDisparar->start(3000);//parametro q cambia de acuerdo con el nivel

    timerMoveBulletEnemy = new QTimer(this);
    connect(timerMoveBulletEnemy, SIGNAL(timeout()), this, SLOT(moveBullet()));
    timerMoveBulletEnemy->start(100);

    sentido = true;
}

Enemy::~Enemy()
{
    timerDisparar->stop();
    timerMoveBulletEnemy->stop();
}

void Enemy::disparar()
{
    bullet2 = new BulletEnemy();
    bullet2->setPx(x());
    bullet2->setPy(y());
    bullet2->setPos(x(),y());
    scene()->addItem(bullet2);

    vBullet.push_back(bullet2);

}

void Enemy::moveBullet()
{
    for (int i = 0; i < vBullet.size(); ++i) {
        vBullet[i]->act_p();
    }

}

void Enemy::paint(QPainter *painter, const QStyleOptionGraphicsItem *, QWidget *)
{
    QPixmap en;
    en.load(":/ByM/ByM/enemy.png");
    painter->drawPixmap(0,0,100,100,en);
}


int Enemy::getVida() const
{
    return vida;
}

void Enemy::setVida(int value)
{
    vida = value;
}

void Enemy::advance(int )
{
    act_p();
}

void Enemy::act_a()
{
    ax = 1;

    if(sentido)
        ay = -GE;
    else
        ay = GE;
}

void Enemy::act_v()
{
    vx += ax*dT+dT*5;
    vy += -ay*dT;

}

void Enemy::setPosition(float _x, float _y)
{
    setPos(_x,_y);
    posx = _x;
    posy = _y;
}
void Enemy::act_p()
{
    act_a();
    act_v();
    posx += 15*sin(vx);
    posy -= vy*dT+0.5*ay*dT*dT;
    setPos(posx,posy);

    checkLimits();
}

void Enemy::checkLimits(void){

    if(posy>250){
        sentido = true;
    }else{
        sentido = false;
    }
    if(posy+100 > 500){
        posy = 400;
        setPos(posx,posy);
    }

}
