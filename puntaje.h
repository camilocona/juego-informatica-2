/*
 * Esta clase hereda las funciones de QGraphicsTextItem
 * con el fin de pintar en pantalla el puntaje del jugador
 */

#ifndef PUNTAJE_H
#define PUNTAJE_H

#include <QGraphicsTextItem>
#include <QDebug>

class puntaje : public QGraphicsTextItem
{
public:
    puntaje(QGraphicsItem * =0);

    //metodos get de las variables
    int getVida() const;
    QString getName() const;

    //metodos set de las variables
    void setVida(int value);
    void setName(const QString &value);

private:
    int vida = 0; //variable que almacena la vida del personaje
    QString name; //variable que almacena el nombre del personaje
};

#endif // PUNTAJE_H
