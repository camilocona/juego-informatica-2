/*
 * Esta clase administra las balas disparadas por el enemy
 * se encuentran los metodos que definen los movimientos de estas
 */

#ifndef BULLETENEMY_H
#define BULLETENEMY_H

#include <QGraphicsRectItem>
#include <QGraphicsItem>
#include <QObject>
#include <QGraphicsScene>
#include <QList>
#include <typeinfo>
#include <QTimer>

#define dT 0.1 //parametro que define el delta del tiempo que rige el movimiento de las balas

class BulletEnemy: public QObject, public QGraphicsRectItem
{
    Q_OBJECT

public:
    BulletEnemy(QGraphicsItem *parent=0);

    //metodos get de las variables
    float getPx() const;
    float getPy() const;
    float getVx() const;
    float getVy() const;
    float getAx() const;
    float getAy() const;

    //metodos set de las variables
    void setPx(float value);
    void setPy(float value);
    void setVx(float value);
    void setVy(float value);
    void setAx(float value);
    void setAy(float value);

    void act_p();//metodos que describen las escuaciones fisicas de movimiento de la bala

public slots:
    void move();

private:
    float px, py, vx, vy, ax, ay; //variables que rigen el movimiento de las balas
    int poder =  10; //poder de la bala con el que destruye al personaje

    //metodos que describen las escuaciones fisicas de movimiento de la bala
    void act_a();
    void act_v();

    //metodo invocado propio de la clase QGraphicsScene que permite ejecutarse al tiempo con las demas clases que utlizan este
    //NO DISPONIBLE ACTUALMENTE
    //void advance(int);
};

#endif // BULLETENEMY_H
