#include "botones.h"

Botones::Botones(int _w, int _h, bool _estado)
{
    w = _w;
    h = _h;
    estado = _estado;
}

QRectF Botones::boundingRect() const
{
    return QRectF(0,0,w,h);
}

void Botones::paint(QPainter *painter, const QStyleOptionGraphicsItem *, QWidget *)
{
    QPen pen;
    pen.setStyle(Qt::NoPen);
    painter->setPen(pen);
    painter->drawRect(boundingRect());
}

void Botones::mousePressEvent(QGraphicsSceneMouseEvent *)
{
    emit click(estado);
}
