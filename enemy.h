/*
 * Esta clase gestiona las acciones relacionadas el enemigo que ataca al personaje
 * incluye los metodos que generan sus movimientos
 * vida y el disparo de balas
 */

#ifndef ENEMY_H
#define ENEMY_H

#include <QGraphicsRectItem>
#include <QObject>
#include <QTimer>
#include <QPainter>
#include <QVector>
#include "bullet.h"
#include "bulletenemy.h"

#define GE 7 //parametro que define la gravedad que rige el movimiento del enemigo

class Enemy: public QObject, public QGraphicsRectItem
{
    Q_OBJECT
public:
    Enemy();
    ~Enemy();

    //metodos get y set de la vida del enemigo
    int getVida() const;
    void setVida(int value);

    //metodo invocado propio de la clase QGraphicsScene que permite ejecutarse al tiempo con las demas clases que utlizan este
    void advance(int);

    QTimer* timerDisparar; //timer utilizado para la generacion de las balas
    QTimer* timerMoveBulletEnemy; //timer utilizado para actualizar las posicion de las balas disparadas y almacenadas en un vector

    BulletEnemy* bullet2; //puntero de tipo BulletEnemy que permite la creacion de las balas
    void act_p(); //metodos que describen las escuaciones fisicas de movimiento de la bala

public slots:
    void disparar(); //slot que controla la creacion de nuevos objetos tipo BulletEnemy y los almacena en el vector vBullet
    void moveBullet(); //slot que ejecuta las funciones que rigen el movimiento de las balas del enemigo

private:
    bool flag = true;
    int vida = 100; //vida del enemigo

    QVector<BulletEnemy *> vBullet; //vector de BulletEnemy que almacena las balas lanzadas por el enemy

    void paint(QPainter *painter, const QStyleOptionGraphicsItem *, QWidget *); //funcion que permite ponerle imagen a las balas
    float posx, posy, vx, vy, ax, ay; //variables que rigen el movimiento de las balas
    bool sentido; //variable requerida para evaluar el sentido de movimiento del enemigo

    //metodos que describen las escuaciones fisicas de movimiento de la bala
    void act_a();
    void act_v();

    void checkLimits(); //metodo que verifica los limites en los que se puede mover el enemigo
public:
    void setPosition(float _x, float _y); //metodo que permite definirle la posicion en la escena al enemigo

};

#endif // ENEMY_H
