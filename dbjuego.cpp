#include "dbjuego.h"

DBJuego::DBJuego()
{
    if(conexionBaseDatos()){
        if(crearBaseDatos()){
            crearTabla();
        }
    }
}

bool DBJuego::conexionBaseDatos()
{
    QSqlQuery query;
    qDebug()<<"Cargando driver";
    if(QSqlDatabase::isDriverAvailable("QSQLITE")){
        qDebug()<<"(1) - Exitoso";
    }else {
        qDebug()<<"(1) - Error";
    }

    return true;
}

bool DBJuego::crearBaseDatos()
{
    baseDatos = QSqlDatabase::addDatabase("QSQLITE");
    baseDatos.setDatabaseName("deanfo.sqlite");

    qDebug()<<"Crear Base de Datos";
    if(baseDatos.open()){
        qDebug()<<"(2) - Exitoso";
    }else {
        qDebug()<<"(2) - Error";
    }
    return true;
}

bool DBJuego::crearTabla()
{
    QString crear;
    QSqlQuery query;

    crear = "CREATE TABLE IF NOT EXISTS jugador("
            "id INTEGER PRIMARY KEY AUTOINCREMENT,"
            "nombre VARCHAR(20) UNIQUE,"
            "nivel INT,"
            "puntaje INT,"
            "vida INT)";

    query.prepare(crear);

    qDebug()<<"Crear Tabla";
    if(query.exec()){
        qDebug()<<"(3) - Exitoso";
    }else {
        qDebug()<<"(3) - Error"<<query.lastError();
    }
    return true;
}

bool DBJuego::insertar(QString _name)
{
    QString enviar;
    QSqlQuery query;

    enviar =    "INSERT INTO jugador (nombre, nivel, puntaje, vida)"
                "VALUES ('"+_name+"',1,0,100)";

    query.prepare(enviar);

    qDebug()<<"Insertar";
    if(query.exec()){
        qDebug()<<"(4) - Exitoso";
        return true;
    }else {
        qDebug()<<"(4) - Error"<<query.lastError();
        return false;
    }
}

QSqlQuery DBJuego::consultar(QString _name)
{
    QString traer;
    QSqlQuery query;
    traer = "SELECT * FROM jugador WHERE nombre = '"+_name+"'";

    query.prepare(traer);

    qDebug()<<"Consultar";
    if(query.exec() && query.next()){
        qDebug()<<"(4) - Exitoso";//<<query.value("nombre").toString();
    }else {
        qDebug()<<"(4) - Error"<<query.lastError();
        //return false;
    }
    return query;
}

bool DBJuego::guardarPartida(QString _name, int _nivel, int _puntaje, int _vida)
{
    QString enviar;
    QSqlQuery query;

    enviar =    "UPDATE jugador SET nivel = '"+ QString::number(_nivel)+"', puntaje = '"+QString::number(_puntaje)
            +"', vida = '"+QString::number(_vida)+"'"+"WHERE nombre='"+_name+"'";

    query.prepare(enviar);

    qDebug()<<"Guardar Partida";
    if(query.exec()){
        qDebug()<<"(4) - Exitoso";
        return true;
    }else {
        qDebug()<<"(4) - Error"<<query.lastError();
        return false;
    }
}
