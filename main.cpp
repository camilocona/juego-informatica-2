#include <QApplication>
#include <game.h>
#include <trivia.h>
Game * game;
Trivia * trivia;

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    trivia = new Trivia();
    trivia->show();

    return a.exec();
}
