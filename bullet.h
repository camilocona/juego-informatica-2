/*
 * Esta clase administra las balas disparadas por el personaje
 * se encuentran los metodos que definen los movimientos de estas
 */

#ifndef BULLET_H
#define BULLET_H

#include <QGraphicsRectItem>
#include <QGraphicsItem>
#include <QObject>
#include <QGraphicsScene>
#include <QList>
#include <typeinfo>
#include <QPainter>

#define dT 0.1 //parametro que define el delta del tiempo que rige el movimiento de las balas

class Bullet: public QObject, public QGraphicsRectItem
{
    Q_OBJECT
public:
    Bullet(QGraphicsItem *parent=0);

    //metodos get de las variables
    float getPx() const;
    float getPy() const;
    float getVx() const;
    float getVy() const;
    float getAx() const;
    float getAy() const;
    bool getAvanzar() const;

    //metodos set de las variables
    void setPx(float value);
    void setPy(float value);
    void setVx(float value);
    void setVy(float value);
    void setAx(float value);
    void setAy(float value);
    void setAvanzar(bool value);

    void actualizar(); //metodo que define el movimiento acelerado horizontal en las balas al oprimir la teclas E


    int rot = -1; //rotacion de la pelota
signals:
    void murio(); //señal que emite cuando muere el enemy

public slots:
    void move(); //slot que administra el movimiento de las balas y su colsion con el enemy o por fuera de la escena

private:
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *, QWidget *); //funcion que permite ponerle imagen a las balas
    float px, py, vx, vy, ax, ay; //variables que rigen el movimiento de las balas
    int poder =  50; //poder de la bala con el que destruye al enemy
    bool avanzar;
    int Nav;

    //metodos que describen las escuaciones fisicas de movimiento de la bala
    void act_a();
    void act_v();
    void act_p();

    //metodo invocado propio de la clase QGraphicsScene que permite ejecutarse al tiempo con las demas clases que utlizan este
    void advance(int);
};

#endif // BULLET_H
