/*
 * Esta clase permite crear un tipo de boton virtual
 * que se representa de manera grafica superpuesta en la escena
 * y cada boton se puede conectar independientemente a un slot
 * definido
 */

#ifndef BOTONES_H
#define BOTONES_H

#include <QObject>
#include <QGraphicsItem>
#include <QPainter>
#include <QMouseEvent>

class Botones : public QObject, public QGraphicsItem
{
    Q_OBJECT
public:
    Botones(int _w, int _h, bool _estado=false); //constructor de la clase que recive los parametros de ancho y largo para definir el tamaño del boton

    //metodos para definir el diseño de los botones
    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);

    void mousePressEvent(QGraphicsSceneMouseEvent *); //metodo que captura la señal de clic del mouse sobre el boton
signals:
    void click(bool=false); //señal del clic del boton
private:
    int w,h; //parametros de ancho y alto del boton
    bool estado; //estado que permite enviar como parametro un bool a los slot (requerido para el modulo de iniciar o cargar una partida)
};

#endif // BOTONES_H
