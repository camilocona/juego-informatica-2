#include "game.h"
#include <QDebug>
#include <QGraphicsLineItem>

Game::Game(QWidget *)
{
    //Crear al escena
    scene = new QGraphicsScene();
    scene->setSceneRect(0,0,1000,500);

    setScene(scene);
    setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setFixedSize(1000,500);

    timer2 = new QTimer();
    connect(timer2,SIGNAL(timeout()),this,SLOT(ponerMonedas()));

    timerGeneral = new QTimer();
    connect(timerGeneral,SIGNAL(timeout()),scene,SLOT(advance()));

    timerBalas = new QTimer();
    connect(timerBalas, SIGNAL(timeout()), this, SLOT(movBalas()));
    control = new QSerialPort();

    dbjuego = new DBJuego();
    rect = NULL;

    txtRegistro = new QLineEdit(this);
    txtIngreso = new QLineEdit(this);
    fondopausa = new Fondos("fondopausa");

    end = new Fondos("end");

    pnt = new puntaje();

    menuInicial();

    //Para la musica
    QMediaPlayer* player = new QMediaPlayer;
    player->setMedia(QUrl::fromLocalFile("://song/song/song.mp3"));
    player->setVolume(50);
    player->play();
}

void Game::menuInicial()
{
    scene->clear();

    fondo1 = new Fondos("fondo1");
    scene->addItem(fondo1);

    txtIngreso->hide();

    txtRegistro->setPlaceholderText("NEW USER");
    txtRegistro->setGeometry(660,200,240,25);
    txtRegistro->show();

    bPlayer = new Botones(150,45);
    bPlayer->setPos(160,160);
    connect(bPlayer,SIGNAL(click()),this,SLOT(menuIngreso()));
    scene->addItem(bPlayer);

    bMPlayer = new Botones(265,45);
    bMPlayer->setPos(160,230);
    connect(bMPlayer,SIGNAL(click()),this,SLOT(close()));
    scene->addItem(bMPlayer);

    bIQuit = new Botones(100,45);
    bIQuit->setPos(160,290);
    connect(bIQuit,SIGNAL(click()),this,SLOT(close()));
    scene->addItem(bIQuit);

    bSignin = new Botones(100,45);
    bSignin->setPos(720,245);
    connect(bSignin,SIGNAL(click()),this,SLOT(onSignin()));
    scene->addItem(bSignin);
}

void Game::mousePressEvent(QMouseEvent *m)
{
    //qDebug()<<"x: "<<m->x()<<" y: "<<m->y();
}

void Game::conexion()
{
    control->setPortName("COM3");
    if(control->open(QIODevice::ReadWrite)){
        //Ahora el puerto seria está abierto
        if(!control->setBaudRate(QSerialPort::Baud9600))
            qDebug()<<control->errorString();

        if(!control->setDataBits(QSerialPort::Data8))
            qDebug()<<control->errorString();

        if(!control->setParity(QSerialPort::NoParity))
            qDebug()<<control->errorString();

        if(!control->setStopBits(QSerialPort::OneStop))
            qDebug()<<control->errorString();

        if(!control->setFlowControl(QSerialPort::NoFlowControl))
            qDebug()<<control->errorString();
        timerBalas->start(50);
    }
    else{
        qDebug()<<"Serial COM3 not opened. Error: "<<control->errorString();
    }
}

void Game::createLevel(bool e){

    //Removes que se ejecutan solo la primera vez que se va a crear esta escena de la partida
    if(remover){
        scene->removeItem(fondo3);
        scene->removeItem(bParBack);
        scene->removeItem(bNewGame);
        scene->removeItem(bLoadGame);
        scene->removeItem(bParQuit);
        delete fondo3;
        delete bParBack;
        delete bNewGame;
        delete bLoadGame;
        delete bParQuit;
        remover = false;

        txtIngreso->hide();
    }

    fondoppal = new Fondos("fondoppal");
    scene->addItem(fondoppal);

    bArduino = new Botones(55,65);
    bArduino->setPos(180,0);
    connect(bArduino,SIGNAL(click()),this,SLOT(conexion()));
    scene->addItem(bArduino);

    int tiempo;

    //Creacion del personaje
    if(e){
        rect = new MyRect(0,420,loadRect->getRadio(),100,0);
        level = 1;
    }

    else
        rect = new MyRect(loadRect->getPosx(),loadRect->getPosy(),loadRect->getRadio(),loadRect->getVida(),loadRect->getPuntos());
    rect->setPos(0,420);
    rect->setFlag(QGraphicsItem::ItemIsFocusable);
    rect->setFocus();
    scene->addItem(rect);

    connect(rect,SIGNAL(mate()),this,SLOT(newLevel()));


    //Creacion del enemigo
    enemy = new Enemy();
    enemy->setPosition(700,400);
    scene->addItem(enemy);

    scene->addItem(pnt);

    switch (level) {
    case 1:
        tiempo = 100;
        break;
    case 2:
        tiempo = 80;
        break;
    case 3:
        tiempo = 40;
        break;
    default:
        qDebug() << "fin";
        scene->addItem(end);
        break;
    }
    ponerBloques();
    timerGeneral->start(tiempo);
    timer2->start(5000);
    playPause = PLAY;
}

void Game::ponerBloques()
{
    bloque1 = new Bloques();
    bloque2 = new Bloques();
    bloque3 = new Bloques();
    bloque4 = new Bloques();

    bloque1->setPos(135,380);
    bloque2->setPos(373,295);
    bloque3->setPos(213,196);
    bloque4->setPos(514,91);

    scene->addItem(bloque1);
    scene->addItem(bloque2);
    scene->addItem(bloque3);
    scene->addItem(bloque4);
}

void Game::ponerMonedas()
{
    moneda = new Monedas();
    scene->addItem(moneda);
    contMonedas++;
    if(contMonedas>=4){
        timer2->stop();
    }
}

unsigned int Game::getLevel() const
{
    return level;
}

void Game::setLevel(unsigned int value)
{
    level = value;
}

void Game::keyPressEvent(QKeyEvent *event)
{
    if(event->key() == Qt::Key_P){
        menuPause();
    }
    if(event->key() == Qt::Key_Left){
        rect->setVx(-30);
        rect->setSentido(false);
    }else if(event->key() == Qt::Key_Right){
        rect->setVx(30);
        rect->setSentido(true);
    }else  if (event->key()==Qt::Key_Up) {
        if(rect->flagjump){
            rect->setVy(50);
            rect->flagjump = false;
        }
    }else if(event->key() == Qt::Key_Space){
        //Crear un bullet
        if (rect->flagbull) {
            rect->disparar();
            rect->flagbull = false;
        }

    }else if (event->key()== Qt::Key_R){
        rect->bullet->setVx(0);

    }else if(event->key()==Qt::Key_E){
        rect->bullet->setAvanzar(true);
        rect->bullet->setPx(rect->getPosx()+20);
        rect->bullet->setPos(rect->getPosx(), y());
    }
}

void Game::menuIngreso()
{
    scene->clear();

    fondo2 = new Fondos("fondo2");
    scene->addItem(fondo2);

    txtRegistro->hide();

    txtIngreso->setGeometry(300,170,240,25);
    txtIngreso->setPlaceholderText("USER");
    txtIngreso->show();

    bLogin = new Botones(100,20);
    bLogin->setPos(440,220);
    connect(bLogin,SIGNAL(click()),this,SLOT(onLogin()));
    scene->addItem(bLogin);

    bInBack = new Botones(84,20);
    bInBack->setPos(40,16);
    connect(bInBack,SIGNAL(click()),this,SLOT(menuInicial()));
    scene->addItem(bInBack);
}

void Game::menuPartida(void)
{
    scene->clear();

    fondo3 = new Fondos("fondo3");
    scene->addItem(fondo3);

    txtIngreso->hide();

    bParBack = new Botones(82,20);
    bParBack->setPos(58,58);
    connect(bParBack,SIGNAL(click()),this,SLOT(menuIngreso()));
    scene->addItem(bParBack);

    bNewGame = new Botones(225,20,true);
    bNewGame->setPos(375,230);
    connect(bNewGame,SIGNAL(click(bool)),this,SLOT(createLevel(bool)));
    scene->addItem(bNewGame);

    bLoadGame = new Botones(235,20,false);
    bLoadGame->setPos(365,298);
    connect(bLoadGame,SIGNAL(click()),this,SLOT(createLevel()));
    scene->addItem(bLoadGame);

    bParQuit = new Botones(90,20);
    bParQuit->setPos(430,375);
    connect(bParQuit,SIGNAL(click()),this,SLOT(close()));
    scene->addItem(bParQuit);

}

void Game::onSignin()
{
    qDebug()<<txtRegistro->text();
    QMessageBox msgBox;
    if(dbjuego->insertar(txtRegistro->text())){
        msgBox.setText("Usuario registrado con exito");
        msgBox.exec();
    }else{
        msgBox.setText("El usuario ya existe, digite uno nuevo");
        msgBox.exec();
    }
    txtRegistro->clear();
}

void Game::onLogin()
{
    qDebug()<<txtIngreso->text();
    QMessageBox msgBox;
    QSqlQuery query;
    query = dbjuego->consultar(txtIngreso->text());

    if(query.value("nombre").toString() == txtIngreso->text() && txtIngreso->text() != ""){
        loadRect = new MyRect(0,420,40,query.value("vida").toInt(),query.value("puntaje").toInt());
        level = query.value("nivel").toInt();
        user = query.value("nombre").toString();
        menuPartida();
    }
    else {
        msgBox.setText("Usuario no encontrado, por favor registrese");
        msgBox.exec();
    }
    txtIngreso->clear();
}

void Game::newLevel()
{
    level++;

    delete loadRect;

    //Removes
    scene->removeItem(rect);
    scene->removeItem(fondoppal);
    scene->removeItem(pnt);
    scene->removeItem(bloque1);
    scene->removeItem(bloque2);
    scene->removeItem(bloque3);
    scene->removeItem(bloque4);

    //Stop timers
    timerGeneral->stop();
    timer2->stop();
    playPause = PAUSE;

    //Recargar el loadrect
    loadRect = new MyRect(0,420,40,rect->getVida(),rect->getPuntos());
    createLevel();
}

void Game::onSave()
{
    QMessageBox msgBox;

    if(dbjuego->guardarPartida(user, level, rect->getPuntos(), rect->getVida())){
        msgBox.setText("Partida guarda con exito");
        msgBox.exec();
    }else{
        msgBox.setText("Error al guardar la partida");
        msgBox.exec();
    }
}

void Game::menuPause()
{
    if(playPause==PLAY){
        bContinue = new Botones(265,25);
        bContinue->setPos(375,170);
        connect(bContinue,SIGNAL(click()),this,SLOT(menuPause()));

        bSave = new Botones(127,25);
        bSave->setPos(430,233);
        connect(bSave,SIGNAL(click()),this,SLOT(onSave()));

        bPauseQuit = new Botones(117,25);
        bPauseQuit->setPos(428,396);
        connect(bPauseQuit,SIGNAL(click()),this,SLOT(close()));

        scene->addItem(fondopausa);
        scene->addItem(bContinue);
        scene->addItem(bSave);
        scene->addItem(bPauseQuit);

        timerGeneral->stop();
        playPause = PAUSE;
    }else{

        scene->removeItem(fondopausa);

        scene->removeItem(bContinue);
        disconnect(bContinue,SIGNAL(click()),this,SLOT(close()));
        delete bContinue;

        scene->removeItem(bSave);
        disconnect(bSave,SIGNAL(click()),this,SLOT(close()));
        delete bSave;

        scene->removeItem(bPauseQuit);
        disconnect(bPauseQuit,SIGNAL(click()),this,SLOT(close()));
        delete bPauseQuit;

        timerGeneral->start();
        playPause = PLAY;
    }
}

void Game::movBalas()
{
    char data;
    control->read(&data,1);
    if (data == '1') {
        if (rect->flagbull) {
            rect->disparar();
            rect->flagbull = false;
        }
    }
}
