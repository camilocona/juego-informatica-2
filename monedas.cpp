#include "monedas.h"
#include "stdlib.h"

Monedas::Monedas()
{
    int x = rand() % 500;
    setPos(x,480);

}

QRectF Monedas::boundingRect() const
{
    return QRectF(0,0,20,20);
}

void Monedas::paint(QPainter *painter, const QStyleOptionGraphicsItem *, QWidget *)
{
    painter->setBrush(Qt::yellow);
    painter->drawEllipse(boundingRect());
}

int Monedas::getValor() const
{
    return valor;
}

void Monedas::setValor(int value)
{
    valor = value;
}

QPainterPath Monedas::shape() const
{
    QPainterPath path;
    path.addEllipse(boundingRect());
    return path;
}
