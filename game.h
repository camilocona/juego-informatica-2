/*
 * Esta clase es la encargada de administrar todos los modulos del juego
 * gestionar las escenas, personajes y eventos que transcurren en la aplicacion
 * ejecuta la funcion advance y controla el reloj general con el que se mueve el juego
 * administra los diferentes niveles de las partidas y su conexion con el arduino
 */

#ifndef GAME_H
#define GAME_H

#include <QGraphicsView>
#include <QWidget>
#include <QGraphicsScene>
#include <QTimer>
#include <QGraphicsTextItem>
#include <QFont>
#include <QKeyEvent>
#include <QLineEdit>
#include <QMessageBox>
#include <QObject>
#include <QMediaPlayer>

#include "myrect.h"
#include "enemy.h"
#include "bloques.h"
#include "monedas.h"
#include "puntaje.h"
#include "fondos.h"
#include "botones.h"
#include "dbjuego.h"
#include <QtSerialPort/QSerialPort>
#include <QtSerialPort/QSerialPortInfo>

//macros definidas para la funcion de PLAY & PAUSE del juego
#define PLAY true
#define PAUSE false

class Game: public QGraphicsView
{
    Q_OBJECT
public:
    Game(QWidget * =0);

    QGraphicsScene* scene; //puntero referente a la clase QGraphicsScene
    MyRect *rect, *loadRect; //punteros referente a la clase MyRect, el primero creal al personaje de la escena, el segundo se carga como instacia copia con los datos de la DB
    Enemy* enemy; //puntero referente a la clase Enemy
    QTimer* timer2; //timer que pone las monedas en la escena
    QTimer* timerGeneral; //timer que ejecuta el SLOT advance de la clase QGraphicsScene
    QTimer* timerBalas; //timer que ejecuta la accion de disparar las balas, se inicia luego de la conexion con el arduino

    puntaje* pnt; //puntero referente a la clase puntaje para agregarlo a la escena

    Monedas* moneda; //puntero que instancia la clase Monedas para agregarlas a la escena
    int contMonedas = 0; //contador de monedas, controla la cantidad de monedas que salen en cada escena

    //funciones get y set de la variable level
    unsigned int getLevel() const;
    void setLevel(unsigned int value);

    void keyPressEvent(QKeyEvent * event); //metodo que permite la lectura de datos ingresados por el teclado
    void mousePressEvent(QMouseEvent *m); //metodo que permite la lectura de datos ingresados por el mouse

    QSerialPort* control; //puntero de tipo QSerialPort se utiliza para trabajar con las funciones de conexion con el puerto serial

public slots:
    void conexion(); //slot que realizar la conexion con el puerto serial donde se emite la señal del arduino
    void menuInicial(); //slot que recea la escena del menu inicial
    void menuIngreso(); //slot que recea la escena del menu de ingreso
    void menuPartida(void); //slot que recea la escena del menu de partida
    void createLevel(bool e = false); //slot que recea la escena principal del juego
    void onSignin(); //slot que envia la información de registro a la DB
    void ponerMonedas(); //slot que crea y añade las monedas al juego
    void onLogin(); //slot que consulta y carga la información de la DB para ingresar al juego
    void newLevel(); //slot que captura y da el paso al cambio de nivel en el juego
    void onSave(); //slot que actualiza la informacion de la partida partida en la DB
    void menuPause(); //slot que recea la escena del menu de pausa
    void movBalas(); //slot llamado para disparar las balas de personaje por el arduino

private:
    bool remover = true; //bandera que permite ejecutar por unica vez la remocion de la escena anterior a la de la partida
    int level; //varible que lleva el conteo de los niveles
    bool playPause; //variable que lleva la bandera de play o pausa del juego
    QString user; //variable que almacena el nombre del usuario que esta jugando, es cargado por la DB
    QLineEdit *txtRegistro, *txtIngreso; //cajas de texto que almacenan los nombres del jugador que se esta registrando o logueando
    DBJuego* dbjuego; //variable de tipo DBJuego utilizada para usar sus funciones relacionadas con las transacciones con la DB
    Fondos* fondopausa; //varible que instancia la clase Fondos
    Bloques *bloque1, *bloque2, *bloque3, *bloque4; //una instancia por cada plataforma que se dibuja en la escena de juego

    void ponerBloques(); //metodo que administra la creacion e instancia de las plataformas en la escena

    /*intancia general de cada elemento necesario para cada escena*/
    //Menu Inicial
    Fondos* fondo1;
    Botones* bPlayer;
    Botones* bMPlayer;
    Botones* bIQuit;
    Botones* bSignin;

    //Create Level
    Fondos* fondoppal;
    Botones* bArduino;

    //Menu Ingreso
    Fondos* fondo2;
    Botones* bLogin;
    Botones* bInBack;

    //Menu Partida
    Fondos* fondo3;
    Botones* bParBack;
    Botones* bNewGame;
    Botones* bLoadGame;
    Botones* bParQuit;

    //Menu Pausa
    Botones* bContinue;
    Botones* bSave;
    Botones* bPauseQuit;

    //FIN
    Fondos* end;
};

#endif // GAME_H
