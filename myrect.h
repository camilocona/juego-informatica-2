/*
 * Esta clase gestiona la creacion del personaje sus movimientos al interior de la escena
 * valida en que momento puede volver a saltar o disparar
 * y controla su puesta en escena verificando las colisiones que presente con los demas objetos dentro de la escena
 */

#ifndef MYRECT_H
#define MYRECT_H

#include <QPainter>
#include <QKeyEvent>
#include <QGraphicsScene>
#include <QGraphicsRectItem>
#include <QObject>
#include <QList>
#include <typeinfo>

#include "bullet.h"
#include "bloques.h"

#define dT 0.1 //parametro que define el delta del tiempo que rige el movimiento del personaje
#define GR 9.8 //parametro que define la gravedad que rige el movimiento del personaje

class MyRect: public QObject, public QGraphicsRectItem{
    Q_OBJECT
public:
    MyRect(float _posx, float _posy, float _r, int _vida, int _puntos); //constructor del personaje que recibe su posicion en la escena, el tamaño del radio la vida y su puntaje, esta informacion adcionalmente se carga desde la DB
    ~MyRect();

    //metodos para definir el diseño del personaje
    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *, QWidget *);

    bool flagjump; //bandera que no permite realizar un salto hasta que regrese de nuevo al piso
    bool flagbull; //bandera que no permite dispar de nuevo hasta que impacte al enemigo o salga de la escena

    //metodos get de las variables
    float getPosx() const;
    float getPosy() const;
    float getRadio() const;
    float getVx() const;
    float getVy() const;
    float getAx() const;
    float getAy() const;
    int getVida() const;
    int getPuntos() const;
    bool getAvanzar() const;
    bool getSentido() const;

    //metodos set de las variables
    void setPosx(float value);
    void setPosy(float value);
    void setRadio(float value);
    void setVx(float value);
    void setVy(float value);
    void setAx(float value);
    void setAy(float value);
    void setVida(int value);
    void setPuntos(int value);
    void setAvanzar(bool value);
    void setSentido(bool value);

    //metodos que describen las escuaciones fisicas de movimiento del personaje
    void act_a();
    void act_v();
    void act_p();

    //funcion que permite detectar y ajustar las colisiones con diferentes objetos
    QPainterPath shape() const;

    void colisionMoneda(); //metodo que verifica la colision con las monedas y aumenta el puntaje del personaje
    void advance(int); //metodo invocado propio de la clase QGraphicsScene que permite ejecutarse al tiempo con las demas clases que utlizan este
    void disparar(); //metodo que crea y agrega a la escena las balas que dispara el personaje

    Bullet* bullet; //puntero de tipo Bullet que instancia las balas del perosaje
signals:
    void mate(); //señal que recibe cuando el enemigo se muere y sale de la escena

public slots:
    void smate(); //slot que ejecuta la señal de que se destruyo el enemigo

private:
    void checkLimits(); //metodo que verifica que el personaje no se salga de los limites de la escena

    float posx = 0, posy = 460, radio, vx, vy, ax, ay; //variables que rigen el movimiento y el tamaño del personaje
    bool sentido; //variable que verifica el sentido para el que debe moverse el personaje
    int vida; //varible que almacena la vida del personaje
    int puntos; //variable que almacena el puntaje del personaje
};

#endif // MYRECT_H
