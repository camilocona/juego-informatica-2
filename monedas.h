/*
 * Esta clase se encarga de diseñar y generar las monedas
 * que se presentan en la escena con el fin de que el personaje
 * pueda subir su puntaje
 */

#ifndef MONEDAS_H
#define MONEDAS_H

#include <QGraphicsRectItem>
#include <QObject>
#include <QGraphicsScene>
#include <QPainter>

class Monedas: public QObject, public QGraphicsRectItem
{
    Q_OBJECT
public:
    Monedas();

    //metodos para definir el diseño de las monedas
    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *, QWidget *);

    //metodos get y set del valor de las monedas
    int getValor() const;
    void setValor(int value);

    //funcion que permite detectar y ajustar las colisiones con diferentes objetos
    QPainterPath shape() const;

private:
    int valor = 10; //varible que almacena el puntaje de las monedas
};

#endif // MONEDAS_H
