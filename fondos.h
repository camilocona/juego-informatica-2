/*
 * Esta clase gestiona la presentacion de las diferentes imagenes que recrean los modulos y las escenas
 */

#ifndef FONDOS_H
#define FONDOS_H

#include <QGraphicsItem>
#include <QPainter>

class Fondos : public QGraphicsItem
{
public:
    Fondos(QString nom); //constructor que recibe como parametro el nombre del fondo que se desea abrir

    //metodos para definir el diseño de los fondos
    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *, QWidget *);
private:
    QPixmap img; //varible tipo QPixmap para gestionar la presentacion de las imagenes
};

#endif // FONDOS_H
