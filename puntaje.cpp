#include "puntaje.h"
#include "game.h"

puntaje::puntaje(QGraphicsItem *)
{
    //setPlainText(name + QString(": ") + QString::number(vida));
    setPlainText(QString("Puntaje: ") + QString::number(vida));
    setDefaultTextColor(Qt::blue);
    setFont(QFont("times",16));
}

int puntaje::getVida() const
{
    return vida;
}

void puntaje::setVida(int value)
{
    vida = value;
    qDebug () << vida;

}

QString puntaje::getName() const
{
    return name;
}

void puntaje::setName(const QString &value)
{
    name = value;
}
