/*
 * Esta clase se encarga de diseñar y generar las plataformas
 * que se presentan en la escena con el fin de que el personaje
 * pueda subirse a ellas
 */
#ifndef BLOQUES_H
#define BLOQUES_H

#include <QGraphicsRectItem>
#include <QGraphicsScene>
#include <QPainter>

class Bloques: public QObject, public QGraphicsRectItem
{
    Q_OBJECT
public:
    Bloques();
private:
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *, QWidget *); //funcion que permite agregar la imagen de los bloques
};

#endif // BLOQUES_H
